<?php

namespace Drupal\rfn_artists;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\PathValidator;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Render\Renderer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Extension\ExtensionInterface;
use Twig\Extension\AbstractExtension;
use Drupal\Core\StreamWrapper\StreamWrapperManager;

/**
 * Twig extension.
 */
class RfnArtistPlaylistTwigExtension extends AbstractExtension implements ExtensionInterface {

  use StringTranslationTrait;

  /**
   * The storage handler class for nodes.
   *
   * @var \Drupal\node\NodeStorage
   */
  private $nodeStorage;

  /**
   * The storage handler class for nodes.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  private $renderer;

  /**
   * Path validator.
   *
   * @var \Drupal\Core\Path\PathValidator
   */
  private $pathValidator;

  /**
   * Stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManager
   */
  private $streamWrapperManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity
   *   The Entity type manager service.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The Rendererer.
   * @param \Drupal\Core\Path\PathValidator $pathValidator
   *   The Path Validator.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManager $streamWrapperManager
   *   The Stream Wrapper Manager.
   */
  public function __construct(EntityTypeManagerInterface $entity, Renderer $renderer, PathValidator $pathValidator, StreamWrapperManager $streamWrapperManager) {
    $this->nodeStorage = $entity->getStorage('node');
    $this->entityTypeManager = $entity;
    $this->renderer = $renderer;
    $this->pathValidator = $pathValidator;
    $this->streamWrapperManager = $streamWrapperManager;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, EntityTypeManagerInterface $entity, Renderer $renderer, PathValidator $pathValidator, StreamWrapperManager $streamWrapperManager) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $container->get('path.validator'),
      $container->get('stream_wrapper_manager')

      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('playlist_for_artist', function ($artistNid = NULL) {

        $url_object = $this->pathValidator->getUrlIfValid('node/' . $artistNid);
        $route_name = $url_object->getRouteName();

        $node = $this->nodeStorage->load($artistNid);
        $artistName = $node->getTitle();

        $output = '';
        $output .= '<a href="javascript://" id="artist-playlist-toggle"> &raquo; ' . $this->t('Click to listen to all tracks for %artistName', ['%artistName' => $artistName]) . "</a>";
        $output .= '<div class="hidden" id="artist-playlist-close">&laquo;  ' . $this->t('Click to hide playlist for %artistName', ['%artistName' => $artistName]) . '</div>';
        $output .= '<div class="hidden" id="artist-playlist-open">&raquo; ' . $this->t('Click to listen to all tracks for %artistName', ['%artistName' => $artistName]) . '</div>';

        $output .= '<div class="artist-playlist-wrapper collapsed" style="display: none;">';
        // Get all the recordings that belong to this artist.
        $artistAlbumNodes = $this->entityTypeManager
          ->getStorage('node')
          ->loadByProperties(['field_artists' => $artistNid]);

        $tracks = [];

        if ($artistAlbumNodes) {
          foreach ($artistAlbumNodes as $album) {

            if (@$album->field_media_items) {
              foreach ($album->field_media_items as $track) {
                $tracks[] = [
                  'title' => $track->entity->getTitle(),
                  'duration' => $track->entity->get('field_duration')->value ? $track->entity->get('field_duration')->value : '???',
                  'uri' => $track->entity->get('field_media_streaming_uri')->value,
                  'album' => $album->getTitle(),
                ];
              }
            }
          }
        }
        $renderable = [
          '#theme' => 'artist_playlist',
          '#artistName' => $artistName,
          '#tracks' => $tracks,
        ];

        $output .= $this->renderer->renderPlain($renderable);

        $output .= '</div>';
        return $output;
      }),
      new \Twig_SimpleFunction('rfn_build_streaming_uri', function ($uri) {

        $stream = $this->streamWrapperManager->getViaUri('private://' . $uri);
        return $stream->getExternalUrl();
      }),

      new \Twig_SimpleFunction('rfn_playlist', function ($tracks) {

        return "I am a playlist for " . count($tracks) . " tracks";

      }),

    ];
  }

}
