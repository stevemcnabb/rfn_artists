<?php

namespace Drupal\rfn_artists\Controller;

use Drupal\rfn_media_audio\Entity\MediaAudio;
use Drupal\file\Entity\File;

/**
 * Returns responses for RFN Artist routes.
 */
class RfnArtistsAlbumsViewController extends RfnArtistsControllerBase {

  /**
   * {@inheritdoc}
   */
  public function build($node = NULL) {

    $node = $this->entityTypeManager()->getStorage('node')
      ->load($this->routeMatch->getParameter('node'));

    // Get collections for this artist.
    $collections = $node->collections([MediaAudio::COLLECTION_TYPE_ALBUM]);

    $collections_for_display = [];
    foreach ($collections as $collection) {

      $cover = [];
      $cover['title'] = $collection->getTitle();
      $cover['id'] = $collection->id;

      // Get the file uri for the image.
      $cover_art_values = $collection->get('field_cover_art')->getValue();
      $cover_art_mid = array_shift($cover_art_values)['target_id'];
      $file = File::load($cover_art_mid);
      $uri = $file->getFileUri();

      $cover['cover_art'] = $uri;
      $cover['link'] = $collection->toUrl()->toString();

      $collections_for_display[] = $cover;
    }

    $recordings_data = [
      '#theme' => 'artist_recordings',
      '#artistName' => $node->getTitle(),
      '#attached' => [
        'library' => [
          'rfn_artists/rfn_artist',
          'rfn_artists/rfn_artist_albums',
        ],
      ],
      '#albums' => $collections_for_display,
      // '#playlists' => TODO
    ];

    $recordings = \Drupal::service('renderer')->render($recordings_data);

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $recordings,
    ];

    return $build;
  }

}
