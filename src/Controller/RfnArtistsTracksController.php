<?php

namespace Drupal\rfn_artists\Controller;

use Drupal\rfn_collections\PlaylistBuilder;

/**
 * Returns responses for RFN Artist routes.
 */
class RfnArtistsTracksController extends RfnArtistsControllerBase {

  /**
   * {@inheritdoc}
   */
  public function build($title = NULL) {

    $entity = $this->entityTypeManager()->getStorage('node')->load($this->routeMatch->getParameter('node'));
    $tracks = $entity->tracks();
    $renderer = \Drupal::service('renderer');

    $playlist_builder = new PlaylistBuilder($tracks, $entity, $renderer);
    $playlist = $playlist_builder->build();
    $track_count = count($tracks);
    $artist_name = $entity->getTitle();

    $build['playlist'] = [
      '#theme' => 'artist_playlist',
      '#playlist' => $playlist,
      '#track_count' => $track_count,
      '#artist_name' => $artist_name,
      '#attached' => [
        'library' => [
          'rfn_colections/playlist',
        ],
      ],
    ];

    return $build;
  }

}
