<?php

namespace Drupal\rfn_artists\Entity;

use Drupal\node\Entity\Node;
use Drupal\rfn_media_audio\Entity\MediaAudio;

/**
 * Defines the artist entity.
 *
 * @ingroup artist
 *
 * @ContentEntityType(
 *   id = "artist",
 *   label = @Translation("artist"),
 *   base_table = "artist",
 *   revision_table = "artist_revision",
 *   revision_data_table = "artist_field_revision",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "published" = "status",
 *     "owner" = "uid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 * )
 */
class Artist extends Node implements ArtistInterface {

  /**
   * {@inheritDoc}
   */
  public function tracks(): array {

    $tracks = \Drupal::service('entity_type.manager')
      ->getStorage('node')->loadByProperties([
        'type' => 'media_audio',
        'field_artists' => $this->id(),
      ]);

    foreach ($tracks as $track) {

      $track_collections = $track->collections([MediaAudio::COLLECTION_TYPE_ALBUM]);
      $collection = array_shift($track_collections);
      $track->collection_title = $collection->getTitle();
    }

    // Sort the tracks.
    usort($tracks, fn($a, $b) => strcmp($a->getTitle(), $b->getTitle()));

    return $tracks;
  }

  /**
   * {@inheritDoc}
   */
  public function collections($types = [1, 2]): array {

    // Type 1 = Album, Type 2 = Playlist.
    $collections = \Drupal::service('entity_type.manager')
      ->getStorage('node')->loadByProperties([
        'type' => 'collection',
        'field_artists' => $this->id(),
        'field_collection_type' => $types,
      ]);

    return $collections;
  }

  /**
   * {@inheritDoc}
   */
  public function addCollection($collection_nid) {

    $collection = \Drupal::service('entity_type.manager')->getStorage('node')->load($collection_nid);
    $artists = $collection->get('field_artists')->getValue();
    $has_artist = FALSE;
    foreach ($artists as $artist) {
      if ($artist['target_id'] == $this->id()) {
        // This artist is already present on this collection.
        $has_artist = TRUE;
      }
    }
    if (!$has_artist) {
      $artists[] = ['target_id' => $this->id()];
      $collection->set('field_artists', $artists);
      $collection->save();
    }
  }

  /**
   * {@inheritDoc}
   */
  public function removeCollection($collection_nid) {
    $collection = \Drupal::service('entity_type.manager')->getStorage('node')->load($collection_nid);
    $artists = $collection->get('field_artists')->getValue();
    $updated_artists = [];
    foreach ($artists as $artist) {
      if ($artist['target_id'] != $this->id()) {
        $updated_artists[] = $artist;
      }
    }
    $collection->set('field_artists', $updated_artists);
    $collection->save();
  }

  /**
   * {@inheritDoc}
   */
  public function urlInfo($rel = 'canonical', array $options = []) {
  }

  /**
   * {@inheritDoc}
   */
  public function url($rel = 'canonical', $options = []) {
  }

  /**
   * {@inheritDoc}
   */
  public function link($text = NULL, $rel = 'canonical', array $options = []) {
  }

}
