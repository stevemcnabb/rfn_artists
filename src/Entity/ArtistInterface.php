<?php

namespace Drupal\rfn_artists\Entity;

use Drupal\node\NodeInterface;

/**
 * Artist interface.
 */
interface ArtistInterface extends NodeInterface {

  /**
   * Tracks associated with this artist.
   */
  public function tracks():array;

  /**
   * Collections associated with this artist.
   */
  public function collections($types = [1, 2]):array;

  /**
   * Add a collection to this artist.
   */
  public function addCollection($collection_nid);

  /**
   * Remove a collection from this artist.
   */
  public function removeCollection($collection_nid);

}
