(function($, Drupal, drupalSettings) {

  window.playListHidden = 0;
  $('#artist-playlist-toggle').click( function() { 
      $('.artist-playlist-wrapper').slideToggle();
      if(window.playListHidden == 0 ) {

        window.playListHidden = 1;
        var linkText = $('#artist-playlist-close').html();
        $('#artist-playlist-toggle').html(linkText);
        var audioElements = document.getElementsByTagName('audio');
        audioElements[0].play();
      }
      else {

        window.playListHidden = 0;
        var linkText = $('#artist-playlist-open').html();
        $('#artist-playlist-toggle').html(linkText);
      }

})

  function playlistToggle() {
    $('artist-playlist-wrapper').slideToggle();
}


})(jQuery, Drupal, drupalSettings);

// Set up handler to move to the next track when track is done
var audioElements = document.getElementsByTagName('audio');

for(var i = 0; i < audioElements.length; i++) {

        console.log("binding audio element " + i);
        var audio = audioElements[i];
     ///   audio.parentElement.parentElement.classList.remove('playingAudio');
        // And the previous row as well.
    //    audio.parentElement.parentElement.previousElementSibling.previousElementSibling.classList.remove('playingAudio');
        audio.addEventListener('ended', playNext);
        audio.addEventListener('play', onPlay);
}

function onPlay(event) {

    var audioElements = document.getElementsByTagName('audio');
    for(var i = 0; i < audioElements.length; i++) {

        var audio = audioElements[i];
       // audio.parentElement.parentElement.classList.remove('playingAudio');

        // Is this element playing audio?
        if(audio.playing) {
            console.log("audio element " + i + " is playing " );
        }
    }

    var thisElement = event.target;
  //  thisElement.parentElement.parentElement.classList.add('playingAudio');

    console.log('TODO - need a unified way to style playing vs not playing audio elements.');

    // And the previous row as well.
    //thisElement.parentElement.parentElement.previousElementSibling.previousElementSibling.classList.add('playingAudio');
}


function playNext(event) {

    var thisElement = event.target;

    var audioElements = document.getElementsByTagName('audio');
    for(var i = 0; i < audioElements.length; i++) {

        var audio = audioElements[i];
        audio.focus();
      //  audio.parentElement.parentElement.previousElementSibling.previousElementSibling.classList.remove('playingAudio');

    }

    // Where is this player in the list? 
    for(var i = 0; i < audioElements.length; i++) {

        var audio = audioElements[i];
        if(audio == thisElement) {

            var next = i + 1;
            if( i >= audioElements.length - 1 ) {
                next = 0;
            }

            stopAllPlayers();
            audioElements[next].play();
            audioElements[next].scrollIntoView({behavior: "auto", block: "center", inline: "nearest"});
       }
    }
}


function stopAllOtherPlayers(event) {

    var audioElements = document.getElementsByTagName('audio');
    var thisElement = event.target;

    for(var i = 0; i < audioElements.length; i++) {

        var audio = audioElements[i];
        if(audio != thisElement) { // If this element is not the one that was clicked, stop aud
          //  audio.classList.remove('playingAudio');
            audio.pause(); 
        }
        else {
          //  audio.classList.add('playingAudio');
        }
    }
}

function stopAllPlayers() {

    var audioElements = document.getElementsByTagName('audio');
    var thisElement = event.target;

    for(var i = 0; i < audioElements.length; i++) {

        var audio = audioElements[i];
        audio.pause(); 
    }
}


